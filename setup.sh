cd backend
composer install
php artisan cache:clear
php artisan migrate:install
php artisan migrate:refresh --seed

cd ..
cd frontend
npm install
