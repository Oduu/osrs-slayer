import React from 'react';
import '../../css/RightNav.css';
import {Link} from 'react-router-dom';

export default class RightNav extends React.Component {
    static propTypes = {
        
    };

    // constructor(props) {
    //     super(props);
    // }

    render() {
        return (
            <div id="right-nav" className={(this.props.open ? 'flex' : 'hide')}>
                <ul id="link-list">

                    <Link to="/" onClick={this.props.toggleMenu}>
                        <li className="nav-link">Home</li>
                    </Link>

                    <Link to="/setups" onClick={this.props.toggleMenu}>
                        <li className="nav-link">Setups</li>
                    </Link>

                    <Link to="/about" onClick={this.props.toggleMenu}>
                        <li className="nav-link">About</li>
                    </Link>



                </ul>
            </div>
        );
    }
}
