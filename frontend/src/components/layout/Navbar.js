import React from 'react';
import '../../css/Navbar.css';
import Burger from './Burger';

export default class Navbar extends React.Component {
    static propTypes = {
        
    };

    // constructor(props) {
    //     super(props);
    // }

    render() {
        return (
            <div id="navbar">
                <div className="details">
                    <img id="logo" src={process.env.PUBLIC_URL + `/gnome-child.png`} alt="logo" />
                    <h1>Task Mate</h1>
                </div>

                {/* also contains nav links */}
                <Burger />

                
            </div>
        );
    }
}
