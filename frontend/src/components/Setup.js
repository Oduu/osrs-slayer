import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// import { Link } from 'react-router-dom';

import '../css/Setup.css'

export class Setup extends Component {

  constructor(props) {
    super(props);
    console.log(this.props.hiscores);
  }

  render() {

    let inventory = Object.keys(this.props.setup['inventory']).map((key) => {
      return <React.Fragment key={key}>
          {/* <p> {this.props.setup['inventory'][key]['name']} </p> */}
          <img src={process.env.PUBLIC_URL + `/items-icons/${this.props.setup['inventory'][key]['id']}.png`} alt={key} title={this.props.setup['inventory'][key]['name']} />
      </React.Fragment>
    })

    let equipped = Object.keys(this.props.setup['equipped']).map((key) => {
      if(this.props.setup['equipped'][key]) {
        return <React.Fragment key={key}>
            <img className="slot-icon" src={process.env.PUBLIC_URL + `/equip-icons/${key}.png`}  alt={key} />
            <img className="item-icon" src={process.env.PUBLIC_URL + `/items-icons/${this.props.setup['equipped'][key]['id']}.png`} alt={key} title={this.props.setup['equipped'][key]['name']} />
            <p style={ 1==0 ? { color: 'red' } : {} }> {this.props.setup['equipped'][key]['name']} </p>
        </React.Fragment>
      } else {
        return <React.Fragment>
            <img className="slot-icon" src={process.env.PUBLIC_URL + `/equip-icons/${key}.png`} alt={key} />
          {/* 0 id used for item icon for dwarven remains which is representative in game as a null item */}
            <img className="item-icon" src={process.env.PUBLIC_URL + `/items-icons/0.png`} alt={key} title="No item" />
            <p>No item</p>
        </React.Fragment>
      }

    })



    return (

      <div key={this.props.setupNum} className="setup">

        

        <div className="equipped">
          {equipped}
        </div>

        
        <div className="inventory">
          {inventory}
        </div>

      </div>

      

    )

  }

}

const mapStateToProps = state => ({
  hiscores: state.hiscores.hiscores,
});

export default connect(mapStateToProps, { })(withRouter(Setup));


