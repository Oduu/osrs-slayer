import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Redirect  } from 'react-router-dom';
import { fetchTasks, fetchLocations } from '../../actions/taskActions';
import { fetchItems } from '../../actions/itemActions';
import { selectTask } from '../../actions/submitActions';
import InventorySelector from '../InventorySelector';
import EquippedSelector from '../EquippedSelector';
// import TestModal from '../Modal';
import SelectSearch from 'react-select-search';
// import { v4 as uuid_v4 } from "uuid";
import axios from 'axios';

import '../../css/Submit.css'

export class Submit extends Component{

	constructor(props) {
		super(props);
		this.handleTask = this.handleTask.bind(this);
		this.addInvSlot = this.addInvSlot.bind(this);
		this.submitSetup = this.submitSetup.bind(this);

	}

	componentDidMount() {
		this.props.fetchTasks();
		
		
		if(this.props.items.length === 0){
			this.props.fetchItems(); 
		}


		this.props.fetchLocations();

		let selectSearchInputs = document.getElementsByClassName("select-search__input");
		for (let element of selectSearchInputs) {
			element.addEventListener("keyup", event => {
	  		
	  		let itemSelect = (element.value);
	  		if(itemSelect.length >= 3) {
	  			this.setState({itemSelect});	
	  		}

		}); 
		}

	}

	state = {
		task: "",
		inventory_slots: 0,
		itemSelect: "XXX"
	}

	handleTask(task) {
		this.props.selectTask(task);
	}

	ObjectLength( object ) {
	    var length = 0;
	    for( var key in object ) {
	        if( object.hasOwnProperty(key) ) {
	            ++length;
	        }
	    }
	    return length;
	};

	addInvSlot() {
		let inventory_slots= this.state.inventory_slots;
		inventory_slots++;
		this.setState({ inventory_slots })
	}

	submitSetup() {
		let formattedInventory = [];

		for(let key in this.props.setup.inventory) {

			if (this.props.setup.inventory.hasOwnProperty(key)) {
				let slot = {"name": this.props.setup.inventory[key]["item"], "quantity": this.props.setup.inventory[key]["quantity"] }
				// console.log(this.props.setup.inventory[key]["item"])
				// console.log(this.props.setup.inventory[key]["quantity"])
				if(slot["name"] !== null && slot["quantity"] !== null) {
					formattedInventory.push(slot);
				}
			}
		}

		let setup = { "equipped": this.props.setup.equipped, "inventory": formattedInventory };

		console.log(setup);

		if(this.props.setup.task !== null) {
			axios.post(process.env.REACT_APP_API_ROUTE + `/task/name/${this.props.setup.task}/setup`, 
			{
		  		"equipped": this.props.setup.equipped,
		  		"inventory": formattedInventory
			});
		}


	}

	render() {

		const renderInventorySelector = () => {
			let inventory_rows = [];
			
			// validate if 28 items have been selected here

				for (var i = 0; i < this.state.inventory_slots; i++) {
					inventory_rows.push(<InventorySelector key={`inventory-${i}`} />)
				}
			
			return inventory_rows;
		}	

		const renderEquippedSelector = 
			this.props.slots.map( slot => {
				return <EquippedSelector key={`${slot}-selector`} slot={slot} />
			});

		const renderAddSlot = () => {
			

			return <button id="add-slot" onClick={this.addInvSlot}>Add Inventory Slot</button>
		}

		if (this.props.auth) {

			return (

				<div id="submit-page">		

					<h1 className="title">Submit new setup</h1>

					

					<div className="submit-intro">
						<SelectSearch onChange={this.handleTask} options={this.props.tasks} name="task-select" placeholder="Choose your task..." />
						{renderAddSlot()}
					</div>

					<div className="submit-item-grid">



						<div className="equipped-items">

							{renderEquippedSelector}	

						</div>

						<div className="inventory-submit">

							
							{renderInventorySelector()}

							
						</div>



					</div>

					<div id="submit-setup" onClick={this.submitSetup}>
						Post setup
					</div>
					

				</div>

			)



		} else {
			return <Redirect to='/'  />
		}


	}
}

const mapStateToProps = state => ({
	tasks: state.task.tasks,
	items: state.item.items,
	slots: state.item.slots,
	setup: state.submit,
	auth: state.submit.auth
});

export default connect(mapStateToProps, { fetchTasks, fetchItems, selectTask, fetchLocations })(withRouter(Submit));