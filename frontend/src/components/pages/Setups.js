import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchTasks, updateTask, fetchSetup } from '../../actions/taskActions';
import '../../css/Setup-page.css';
import '../../css/search-select.css';

import Setup from '../Setup';
import SelectSearch from 'react-select-search';

export class Setups extends Component {

	constructor(props) {
		super(props);
		this.handleTask = this.handleTask.bind(this);
		this.changeSetup = this.changeSetup.bind(this);
		this.onTaskSubmit = this.onTaskSubmit.bind(this);
	};

	componentDidMount() {
		this.props.fetchTasks();
		// console.log(this.props.tasks);
	};

	state = {
		task: "",
		// setup num using string as SelectSearch package uses string as value
		setupNum: "1",
	};

	handleTask(value) {
		let task = value;
		this.setState({task});
	};

	onTaskSubmit(e) {
		e.preventDefault();
		console.log(this.state.task);
		this.props.fetchSetup(this.state.task);
		this.props.updateTask(this.state.task);

		console.log(this.state.task);
		console.log(this.props.setups);
	};

	changeSetup(value) {
		this.setState({	setupNum: (value) })
	};


	render() {

		let errorMessage = () => {

			if(this.props.selected_task.length > 0 && this.props.setups.length === 0 ) {
				return  <div class="alert"><span class="closebtn" onClick={() => this.props.updateTask("")}>&times;</span>{`Error: No setups found for ${this.props.selected_task}`}</div> 
			}

		}

		let setups = Object.keys(this.props.setups).map((key) => {
			if(key === this.state.setupNum) {
				return <div key={key}>
				  	<Setup setupNum={key} setup={this.props.setups[key]} />
				</div>
			} else {
				// just to remove warning
				return null
			}
		})

		const renderSetupSelector = () => {

			if(this.props.setups.length === 0) {

				return <div className="seperator" />
			
			} else {

				return <div id="setup-selector">
					<SelectSearch
						key="setup-selector"
						onChange={this.changeSetup}
						options={
							Object.keys(this.props.setups).map((key) => {
								return {id:key, name: key, value: key}
							})
						} 
						name="setup-selector" 
						placeholder={this.state.setupNum} 
					/>
				</div>						

			}

		}

		return (



			<div id="setups-page">			
				{/*<h1 className="page-title">Setups</h1>*/}
				

				

				<div className="setup-dropdowns">

					{renderSetupSelector()}

					<form onSubmit={this.onTaskSubmit} className="task-form">
						<SelectSearch search onChange={this.handleTask} options={this.props.tasks} name="task-select" placeholder="Task..." />
						<button type="button" onClick={this.onTaskSubmit} id="task-submit-button"><i className="fa fa-search"></i></button>
					</form>

					

				</div>


				{setups}
				{errorMessage()}
				

		
			</div>

		)

	}


}

const mapStateToProps = state => ({
	hiscores: state.hiscores.hiscores,
	setups: state.task.setups,
	tasks: state.task.tasks,
	selected_task: state.task.selected_task
	
});

export default connect(mapStateToProps, { fetchSetup, fetchTasks, updateTask })(withRouter(Setups));


