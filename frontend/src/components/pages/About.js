import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import '../../css/About.css'

export class About extends Component{

	constructor(props) {
		super(props);
		this.showSlides = this.showSlides.bind(this);
	}

	state = {
		slides: ["react", "laravel"],
		slideIndex: 0,
	}

	componentDidMount() {
		this.showSlides();	
	};



	showSlides() {

		let i = 0;

		setInterval( () => {
			this.setState({ slideIndex: i })
			i++
			if(i === this.state.slides.length) {
				i=0
			}
		}, 1500);

	}

	renderSlideshow() {
		return <div id="react" className="tech-image">
			<p>{this.state.slides[this.state.slideIndex]}</p>
		    <img alt="tech" src={process.env.PUBLIC_URL + `/${this.state.slides[this.state.slideIndex]}.png`} />		    
		</div>
	}

	render() {

		return (

			<div id="about-page">


				<div className="site-description">

					<p>This site was made using a React frontend with a Laravel backend.</p>
					<p>Drops down are from the react-select-search package.</p>
					<p>Item details and icons were obtained through the osrsbox-db box static json API.</p>
					
					<p>Any other information such as list of tasks are from the OSRS wiki.</p>

				</div>


				<div className="resources-grid">

					

					<div className="tech-image link" onClick={() => window.open("https://www.osrsbox.com/projects/osrsbox-db/#the-osrsbox-static-json-api")}>
						<p>OSRS BOX</p>
						<img alt="osrsbox" src={process.env.PUBLIC_URL + "/osrsbox.png"} />
					</div>

					{this.renderSlideshow(this.state.slideIndex)}

					<div className="tech-image link" onClick={() => window.open("https://oldschool.runescape.wiki/")}>
						<p>OSRS Wiki</p>
						<img alt="wiki_logo" src={process.env.PUBLIC_URL + "/wiki_logo.png" } />
					</div>

				</div>				

			</div>

		)
	}
}

const mapStateToProps = state => ({
	
});

export default connect(mapStateToProps, { })(withRouter(About));