import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchHiscores } from '../../actions/hiscoresActions';
// import { Link } from 'react-router-dom';

import '../../css/Hiscores.css'

export class Hiscores extends Component{

	state = {
		username: "",
		loading: false
	};

	constructor(props) {
		// must call super constructor before using 'this' in derived class constructor
		super(props);
		this.onHiscoresSubmit = this.onHiscoresSubmit.bind(this);
		this.handleUsername = this.handleUsername.bind(this);
	};

	handleUsername(e) {
		let username = this.state.username;
		username = e.target.value;
		this.setState({username});
		
	};
	
	onHiscoresSubmit(e) {
	  e.preventDefault();
	  this.props.fetchHiscores(this.state.username);

	  // loading animation logic here (probably needs to be in redux or link to action)
	  // let loading = true;
	  // this.setState({loading});
	}


	render() {

		let stats = Object.keys(this.props.hiscores).map((stat) => {
			var item = this.props.hiscores[stat];
			// react fragment used as to work with css grid need to return multiple elements
			return <React.Fragment>
				{/*<h1 className="capitalize" key={stat}>{stat}</h1>*/}
				<img className="stat-icon" src={process.env.PUBLIC_URL + `/stat-icons/${stat}.png`} alt="skill-icon" />
				<p className="skill-info" key={item.level}>{item.level}</p>
				<p className="skill-info" key={item.rank}>{item.rank}</p>
				<p className="skill-info" key={item.exp}>{item.exp}</p>
			</React.Fragment>
					
		});

		const renderLoading = () => {
			if(this.props.loading) {

				return <div id="loading"></div>
			} else {
				return null
			}
		}

		return (

			<div id="hiscores-page">

			{renderLoading()}

			<div className={(this.props.loading ? 'dim-wrapper' : null)}>

				<div className="hiscores-intro">
			
					<h1 id="hiscores-title">Hiscores</h1>

					<h1> - </h1>

					<form onSubmit={this.onHiscoresSubmit} className="username-form">
						<button type="button" onClick={this.onHiscoresSubmit} id="username-button"><i className="fa fa-search"></i></button>
						<input type="text" onChange={this.handleUsername} id="username-input" placeholder="Enter username..." />
					</form>

				</div>

					<div className="hiscores-table">

						<h1>Skill</h1>
						<h1>Level</h1>
						<h1>Rank</h1>
						<h1>Experience</h1>
						{stats}
					</div>

			</div>

		</div>

		)
	}

}

const mapStateToProps = state => ({
	hiscores: state.hiscores.hiscores,
	loading: state.hiscores.loading
});

export default connect(mapStateToProps, { fetchHiscores })(withRouter(Hiscores));