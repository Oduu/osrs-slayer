import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import { fetchHiscores } from '../../actions/hiscoresActions';
import { fetchTasks } from '../../actions/taskActions';


import '../../css/Home.css'

export class Home extends Component{

	componentDidMount() {
		// console.log("home has been loaded");
		// this.props.fetchTasks();
	}

	render() {
		return (

			<div id="home-page">

			<div id="showcase">
				<h1>An OSRS slayer task assistant app</h1>
				<h6>Select one of the options below to get started</h6>
			</div>
			
				<div className="grid-container">

					<div className="grid-item">
						<Link to="/setups">
							<img src={process.env.PUBLIC_URL + `/slayer.png`} alt="setups-link" />
						</Link>
						<p className="page-name">Setups</p>
					</div>
				

					<div className="grid-item">
						<Link to="/hiscores">
							<img src={process.env.PUBLIC_URL + `/skills.png`} alt="hiscores-link" />
						</Link>
						<p className="page-name">Hiscores</p>
					</div>				

					<div className="grid-item">
						<Link to="/submit">
							<img src={process.env.PUBLIC_URL + `/achievement.png`} alt="submit-link" />
						</Link>
						<p className="page-name">Submit</p>
					</div>

				</div>

			</div>

		)
	}
}

const mapStateToProps = state => ({
	hiscores: state.hiscores.hiscores
});

export default connect(mapStateToProps, { fetchHiscores, fetchTasks })(withRouter(Home));