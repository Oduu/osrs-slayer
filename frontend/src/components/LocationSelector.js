import React, { Component } from 'react';
import SelectSearch from 'react-select-search';
import { connect } from 'react-redux';
import '../css/LocationSelector.css';


export class LocationSelector extends Component {

	constructor(props) {
		super(props);
		

	}

	state = {
		location: null
	}

	componentDidMount() {

	}


	render() {
		if(this.state.render) {
		return (
			    
				<div className="location-selector">
					
					<SelectSearch
						search={true} 
						onChange={"hello"}
						
						options={this.props.locations}
						name="location-selector" placeholder={"Choose your location..."} />

				</div>
				
		)
		} else {
			return null;
		}
	}
}

const mapStateToProps = state => ({
	locations: state.task.locations
});

export default connect(mapStateToProps, { })(LocationSelector);