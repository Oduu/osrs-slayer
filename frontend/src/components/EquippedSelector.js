import React, { Component } from 'react';
import SelectSearch from 'react-select-search';
import { connect } from 'react-redux';
import { updateEquipped } from '../actions/submitActions';


export class EquippedSelector extends Component {

	componentDidMount() {

		let selectSearchInputs = document.getElementsByClassName("select-search__input");
			for (let element of selectSearchInputs) {
				element.addEventListener("keyup", event => {
		  		
		  		let itemSelect = (element.value);
		  		if(itemSelect.length >= 3) {
		  			this.setState({itemSelect});	
		  		}

			}); 
		}
	}

	updateSelection(slot, item) {
		this.props.updateEquipped(slot, item);
	}

	state = {
		itemSelect: "XXX",
		items: []
	}

	render() {
		return (
			<div className="equipped-submit" key={`equip-${this.props.slot}`}>

				<img alt="slot_icon" src={process.env.PUBLIC_URL + `/equip-icons/${this.props.slot}.png`} />

				<SelectSearch
				search={true}
				onChange={(item) => this.updateSelection(this.props.slot, {name: item})}
				options={this.props.items.filter(item => item.name.toLowerCase()
					.includes(this.state.itemSelect.toLowerCase()))
					.filter(item => item.equipable === 1)
					.filter(item => item.slot === this.props.slot)} 
				name="task-select"
				placeholder="Type 3 letters to search..." 
				/>

			</div>	

		)
	}

}

const mapStateToProps = state => ({
	items: state.item.items
});


export default connect(mapStateToProps, { updateEquipped })(EquippedSelector);