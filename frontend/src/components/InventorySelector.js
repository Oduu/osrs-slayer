import React, { Component } from 'react';
import SelectSearch from 'react-select-search';
import { connect } from 'react-redux';
import { updateInv, removeSlot } from '../actions/submitActions';
import '../css/InventorySelector.css';
import { v4 as uuid_v4 } from "uuid";

export class InventorySelector extends Component {

	constructor(props) {
		super(props);
		this.handleUnmount = this.handleUnmount.bind(this);

	}

	state = {
		itemSelect: "XXX",
		render: true,
		inventory: { item: null, quantity: null },
		position: null,
		options: [	{"name":"1","value":"1"},{"name":"2","value":"2"},
					{"name":"3","value":"3"},{"name":"4","value":"4"},{"name":"5","value":"5"},
					{"name":"6","value":"6"},{"name":"7","value":"7"},{"name":"8","value":"8"},
					{"name":"9","value":"9"},{"name":"10","value":"10"},{"name":"11","value":"11"},
					{"name":"12","value":"12"},{"name":"13","value":"13"},{"name":"14","value":"14"},
					{"name":"15","value":"15"},{"name":"16","value":"16"},{"name":"17","value":"17"},
					{"name":"18","value":"18"},{"name":"19","value":"19"},{"name":"20","value":"20"},
					{"name":"21","value":"21"},{"name":"22","value":"22"},{"name":"23","value":"23"},
					{"name":"24","value":"24"},{"name":"25","value":"25"},{"name":"26","value":"26"},
					{"name":"27","value":"27"},{"name":"27","value":"28"}]
	}

	componentDidMount() {

		let selectSearchInputs = document.getElementsByClassName("select-search__input");
			for (let element of selectSearchInputs) {
				element.addEventListener("keyup", event => {
		  		
		  		let itemSelect = (element.value);
		  		if(itemSelect.length >= 3) {
		  			this.setState({itemSelect});	
		  		}

			}); 
		}

		let position = uuid_v4();

		this.setState({ position }, () => {
			this.props.updateInv(this.state.position, this.state.inventory);	
		})

		

	}

	handleChange(item, quantity) {
		
		let inventory = { item: item, quantity: quantity };
		this.setState({inventory: inventory}, () => {
			this.props.updateInv(this.state.position, this.state.inventory)
		});
		
	}

	handleUnmount() {
		this.setState({ render: false });
		this.props.removeSlot(this.state.position);
	}



	render() {
		if(this.state.render) {
		return (
			    
				<div className="inventory-item">
					
					<SelectSearch
						search={true} 
						onChange={(item) => this.handleChange(item, this.state.inventory.quantity)}
						// https://gitlab.com/Oduu/osrs-slayer/-/commit/4f2f2013735deb6c7c1700dd5e78b318340e7fb4 for change to not include item-id
						options={this.props.items.filter(item => item.name.toLowerCase().includes(this.state.itemSelect.toLowerCase()))}
						name="item-select" placeholder={this.props.item ? "Choose your item...": this.props.item_name } />
					
					<SelectSearch
						search={true} 
						onChange={(quantity) => this.handleChange(this.state.inventory.item, quantity)}
						options={this.state.options}
						name="item-select" placeholder={"Select number of slots..."} />

					<button className="remove-slot" onClick={this.handleUnmount}>X</button>

				</div>
				
		)
		} else {
			return null;
		}
	}
}

InventorySelector.defaultProps = {
  item_name: 'Please select an item...'
};

const mapStateToProps = state => ({
	items: state.item.items
});

export default connect(mapStateToProps, { updateInv, removeSlot })(InventorySelector);