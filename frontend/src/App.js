import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import Home from './components/pages/Home';
import Setups from './components/pages/Setups';
import Hiscores from './components/pages/Hiscores';
import Navbar from './components/layout/Navbar';
import About from './components/pages/About';
import Submit from './components/pages/Submit';

import store from './store';
// import axios from 'axios';

import './css/App.css';

function App() {
  return (
    <Provider store={store}>
      <Router basename={`${process.env.PUBLIC_URL}`}>
        <Navbar />
        <div className="App">
            
            <div className="main">
              <Route exact path="/" component={Home} />   
              <Route exact path="/setups" component={Setups} />
              <Route exact path="/hiscores" component={Hiscores} />
              <Route exact path="/about" component={About} />
              <Route exact path="/submit" component={Submit} />

              {/* <Route component={About} /> */}

            </div>
          </div>
      </Router>
    </Provider>
       
  );
}

export default App;


