import { FETCH_TASKS, UPDATE_TASK, FETCH_LOCATIONS, FETCH_SETUP } from '../actions/types';
import axios from 'axios';

export const fetchTasks = () => dispatch => {
		
		axios.get(process.env.REACT_APP_API_ROUTE + '/task')
		.then(res => {
			for (let data of res.data) {
				data["value"] = data["name"];
			}
			dispatch({
				type:FETCH_TASKS,
				payload: res.data
			})
		}
	);
};

export const updateTask = (task_name) => dispatch => {
	dispatch({
		type: UPDATE_TASK,
		payload: task_name
	})
};

export const fetchLocations = () => dispatch => {

	axios.get(process.env.REACT_APP_API_ROUTE + '/location')
	.then(res => {
		dispatch({
			type: FETCH_LOCATIONS,
			payload: res.data
		})
	})

}

export const fetchSetup = (task_name) => dispatch => {
	
	axios.get(process.env.REACT_APP_API_ROUTE + `/task/name/${task_name}/setup`)
	.then(res =>

		(res.status === 204 ? dispatch({ type:FETCH_SETUP, payload: [] }) : dispatch({ type:FETCH_SETUP, payload: res.data }))

	)
  	.catch(function (error) {
    	if (error.response) {
      	// Request made and server responded
      		console.log("Request made and server responded")
    	} else if (error.request) {
      	// The request was made but no response was received
      		console.log("The request was made but no response was received")
    	} else {
      	// Something happened in setting up the request that triggered an Error
      		console.log("Something happened in setting up the request that triggered an Error")
    	}
  	});
};