import { FETCH_HISCORES, HISCORES_LOADING } from '../actions/types';
import axios from 'axios';

export const fetchHiscores = (username) => dispatch => {
		
		dispatch({
			type: HISCORES_LOADING
		})		
		axios.get(process.env.REACT_APP_API_ROUTE + `/character/username/${username}/skill`)
		.then(res => {
			dispatch({
				type:FETCH_HISCORES,
				payload: res.data
		
			})
			dispatch({
				type: HISCORES_LOADING
			})	
		}	
	)
  	.catch((error) => {
    	if (error.response) {
      	// Request made and server responded
      		console.log("Request made and server responded")
      		// console.log(error.response.data);
      		// console.log(error.response.status);
	      	// console.log(error.response.headers);

    	} else if (error.request) {
      	// The request was made but no response was received
      		// console.log("The request was made but no response was received")
      		// console.log(error.request);
    	} else {
      	// Something happened in setting up the request that triggered an Error
      		console.log("Something happened in setting up the request that triggered an Error")
      		// console.log('Error', error.message);
    	}

    	    dispatch({
			type: HISCORES_LOADING
		})

  	});
};