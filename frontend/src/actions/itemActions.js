import { FETCH_ITEMS } from '../actions/types';
import axios from 'axios';

export const fetchItems = () => dispatch => {
		
		axios.get(process.env.REACT_APP_API_ROUTE + '/item')
		.then(res => {
			for (let data of res.data) {
				data["value"] = data["name"];
			}
			dispatch({
				type: FETCH_ITEMS,
				payload: res.data
			})
		}
	);
};