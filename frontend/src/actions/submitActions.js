import { UPDATE_INV, UPDATE_EQUIPPED, REMOVE_SLOT, SELECT_TASK } from '../actions/types';


export const updateInv = (id, inventory) => dispatch => {

		dispatch({
			type:UPDATE_INV,
			id: id,
			inventory: inventory,
		})
};

export const updateEquipped = (slot, item) => dispatch => {
	// console.log("update equipped action called")
	dispatch({
		type: UPDATE_EQUIPPED,
		slot: slot,
		item: item
	})
}

export const removeSlot = (id) => dispatch => {
	dispatch({ 
		type: REMOVE_SLOT,
		id: id
	})
}

export const selectTask = (task) => dispatch => {
	dispatch({
		type: SELECT_TASK,
		task: task
	})
}