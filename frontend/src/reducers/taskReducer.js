import { FETCH_TASKS, FETCH_SETUP, UPDATE_TASK, FETCH_LOCATIONS } from '../actions/types';

const initialState = {
	tasks: [],
	setups: [],
	locations: [],
	selected_task: ""
	
};

export default function(state = initialState, action) {
	switch (action.type) {
		case FETCH_TASKS:
			return {
				...state,
				"tasks": action.payload
			};
		case FETCH_LOCATIONS: 
			return {
				...state,
				"locations": action.payload
			}
		case FETCH_SETUP:
			
			return {
				...state,
				"setups": action.payload
			};
		case UPDATE_TASK:
			
			return {
				...state,
				"selected_task": action.payload
			};
		default:
			return state;
	}
}