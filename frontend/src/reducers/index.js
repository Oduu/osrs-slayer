import { combineReducers } from 'redux';
import hiscoresReducer from './hiscoresReducer';
import taskReducer from './taskReducer';
import itemReducer from './itemReducer';
import submitReducer from './submitReducer';

export  default combineReducers({
	hiscores: hiscoresReducer,
	task: taskReducer,
	item: itemReducer,
	submit: submitReducer
});