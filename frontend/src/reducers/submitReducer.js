import { UPDATE_INV, UPDATE_EQUIPPED, REMOVE_SLOT, SELECT_TASK } from '../actions/types';

const initialState = {
	task: null,

	inventory: {},

	equipped: {
	},

	auth: true
};

export default function(state = initialState, action) {
	switch (action.type) {
		case UPDATE_INV:
			return {
				...state,
				inventory: { ...state.inventory, [action.id] :action.inventory }
			};

		case REMOVE_SLOT:
			let key = action.id;
			const { [key]: value, ...newInventory } = { ...state.inventory };
			return {
				...state,
				inventory: newInventory
			}

		case UPDATE_EQUIPPED:
			return {
				...state,
				equipped: { ...state.equipped, [action.slot]: action.item }
			}

		case SELECT_TASK:
			return {
				...state,
				task: action.task
			}
		default:
			return state;
	}
}