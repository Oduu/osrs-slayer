import { FETCH_ITEMS } from '../actions/types';

const initialState = {
	items: [],
		
	slots: ["head", "ammo", "body", "legs", "feet", "hands", "neck", "cape", "ring", "weapon", "shield"]
};

export default function(state = initialState, action) {
	switch (action.type) {
		case FETCH_ITEMS:
			console.log("FETCH ITEMS REDUCER CALLED");
			return {
				...state,
				"items": action.payload
			};
		default:
			return state;
	}
}