import { FETCH_HISCORES, HISCORES_LOADING } from '../actions/types';

const initialState = {
	hiscores: [],
	loading: false
}

export default function(state = initialState, action) {
	switch (action.type) {
		case FETCH_HISCORES:
			return {
				...state,
				hiscores: action.payload
			};
		case HISCORES_LOADING:
			console.log("HISCORES_LOADING")
			console.log(state.loading)
			return {
				...state,
				loading: !state.loading
			}
		default:
			return state;
	}
}