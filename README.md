# OSRS Slayer Assistant

## Features

Core features: Get Setups, Hiscores, Submit new setup

  
    - Home Page
        - Navigation area to core features

![](https://i.imgur.com/oZdM6KN.png)
        
    - Hiscores
        - This retrieves the specified character's in game levels from the Jagex API
        - The details are then stored in a MySQL database with a timestamp
        - For every subsequent request of this user's hiscores, if the timestamp is older than 2 hours then the details are requested again and updated
        - This is helpful as the Jagex API tends to be quite slow (especially with the issues it has been having since Dec 2019)
    
![](https://i.imgur.com/pCkeXqk.jpg)
    
    - Setup
        - This page contains the equipped and inventory items that the user should take to complete their task efficiently
        - If you don't know what an inventory item is, just hover over it to see the item name in the tooltip
        
![](https://i.imgur.com/fv7Y1nt.jpg)
        
    - Submit new setup
        - If a user finds that a setup is missing from their desired task they can add the information for other players to use here
        - The page is split into two sections, one for equipment and one for inventory (mirroring the setup page layout)
        - Using the react-select-search library for the dropdowns, users can select any item and quantity to take on their journey and can add extra inventory slots if required.
        
![](https://i.imgur.com/kOCWJRX.jpg)

Information is stored using Redux.
        
        
    
    

