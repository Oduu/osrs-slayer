<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Repositories\TaskRepository;
use App\Repositories\ItemRepository;
use App\Repositories\TaskItemRepository;

class TaskItemController extends Controller
{
    public function __construct(TaskRepository $taskRepository, ItemRepository $itemRepository, TaskItemRepository $taskItemRepository) {
    	$this->taskRepo = $taskRepository;
    	$this->itemRepo = $itemRepository;
    	$this->taskItemRepo = $taskItemRepository;
    }

    public function show($field, $criteria) {
        // if task exists 
        $task = $this->taskRepo->get($field, $criteria);

        if($task) {

            $task_id = $task['id'];
            $setup_exist = $this->taskItemRepo->getWithNames($task_id);

                // check for get with names here change get with names if needed to reduce db calls
            
            if($setup_exist) {

                return $setup_exist;
                
            } else {

                return Response::json(["error"=> "Task found but no setups"], 204);

            }

        } else {
            return Response::json(["error"=> "Could not find task/setup with given criteria"], 204);
        }

    	
    }

    public function create(Request $request, $field, $criteria) {

    	// table struc - [ task_id, item_id, setup, equipped ]

        $task = $this->taskRepo->get($field, $criteria);

        $check_inventory_size = 0;

        if(isset($request->json()->all()['inventory'])) {

            foreach ($request->json()->all()['inventory'] as $key => $item) {
                $check_inventory_size += $item['quantity'];
            }            

            if ($check_inventory_size <= 28) {

                if($task) {

                    $task_id = $task['id'];

                    $stored_setups = $this->taskItemRepo->numberOfSetups($task_id);

                    $new_setup_num = $stored_setups+1;

                    $get_ids = $this->get_ids($request);

                    $inventory =  $get_ids['inventory'];
                    
                    $equipped =  $get_ids['equipped'];

                    $items_for_db = [];

                    foreach ($inventory as $item) {
                        for ($i=0; $i < $item["quantity"]; $i++) { 
                            array_push($items_for_db, ['task_id' => $task_id, 'item_id' => $item["id"], 'setup' => $new_setup_num, 'equipped'=> 0]);    
                        }
                    }

                    foreach ($equipped as $item) {

                        array_push($items_for_db, ['task_id' => $task_id, 'item_id' => $item["id"], 'setup' => $new_setup_num, 'equipped'=> 1]);
                    }

                    $insert_setup = $this->taskItemRepo->insert_setup($items_for_db);

                    return $this->taskItemRepo->getWithNames($task_id);

                // task wasn't found
                }  else {

                    return Response::json(["error"=> "No task found"], 415);

                } 
                
            } else {
                return Response::json(["error"=> "Inventory size too large, max 28 slots"], 422);
            }
        } else {
            return Response::json(["error"=> "Inventory not supplied"], 422);
        }




    }

    public function get_ids(Request $request) {

        // return "hello";
        
        $inventory = $request->json()->all()['inventory'];
        $equipped = $request->json()->all()['equipped'];

        $names = [];
        foreach ($inventory as $key => $value) {
            array_push($names, $value['name']);
        }

        foreach ($equipped as $key => $value) {
            array_push($names, $value['name']);
        }


        $db_details = $this->itemRepo->getIn($names);

        foreach ($inventory as $key => $item) {
            foreach ($db_details as $db_item) {
                if($item['name'] === $db_item['name']) {
                    $inventory[$key]['id'] = $db_item['id'];
                }
            }
        }

        foreach ($equipped as $key => $item) {
            foreach ($db_details as $db_item) {
                if($item['name'] === $db_item['name']) {
                    $equipped[$key]['id'] = $db_item['id'];
                }
            }
        }

        $data_with_keys = [ "equipped"=> $equipped, "inventory"=> $inventory ];

        
        return $data_with_keys;

    }
    
}


