<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SkillRepository;

class SkillController extends Controller
{
    public function __construct(SkillRepository $skillRepository) {
        $this->skillRepo = $skillRepository;
    }

    public function index() {
    	$skills = $this->skillRepo->getAll();

    	return $skills;
    }

    public function show($field, $criteria) {

    	$valid_fields = ['id', 'name'];

    	if(in_array($field, $valid_fields)) {

    		return $this->skillRepo->get($field, $criteria);		
    	} else {
    		
    		// use response method with first parameter as content and second parameter as HTTP status code
    		return response(['error'=> 'please select provide a valid field'], 400);
    	}

    }
}
