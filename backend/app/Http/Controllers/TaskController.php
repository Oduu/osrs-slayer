<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TaskRepository;

class TaskController extends Controller
{


    public function __construct(TaskRepository $taskRepository) {
        
        $this->taskRepo = $taskRepository;
        
    }

    public function index(){
    	return $this->taskRepo->getAll();
    }

    public function show($field, $criteria) {

    	$valid_fields = ['name', 'id'];

    	if(in_array($field, $valid_fields)) {
	    	$result = $this->taskRepo->get($field, $criteria);
	    	if($result) {
	    		return $result;
	    	} else {
	    		return response($result, 204);
	    	}
    	} else {
    		return response(['error'=> 'invalid field provided']);
    	}

	}
}
