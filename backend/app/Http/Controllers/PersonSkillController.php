<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Repositories\PersonRepository;
use App\Repositories\SkillRepository;
use App\Repositories\PersonSkillRepository;

use Illuminate\Support\Facades\Http;

class PersonSkillController extends Controller
{
	public function __construct(SkillRepository $skillRepository, PersonRepository $personRepository, PersonSkillRepository $personSkillRepository) {
        $this->skillRepo = $skillRepository;
        $this->personRepo = $personRepository;
        $this->personSkillRepo = $personSkillRepository;
    }

    public function apiFetchLynx($name) {

      $skills = $this->skillRepo->getAll();

      // sample data in case need of test and server down
      // $data = "   76427,2041,164522624 228206,94,7977858 219037,92,6645371
      //    277657,98,12732173 178958,99,16040951 137824,99,15733956
      //    86611,88,4491703 209203,96,9743688 52236,99,13424707 134726,87,4189049
      //    133175,94,7973984 106149,91,5947903 326881,81,2195860 123764,85,3311529
      //    61737,89,4844940 324769,73,1032281 99198,86,3748860 73121,85,3276059
      //    155619,80,1986793 202628,89,4883472 4691,99,28548067 99542,77,1628524
      //    180807,78,1638937 189669,82,2525959 -1,-1 -1,-1 -1,-1 415795,33 -1,-1
      //    549018,3 421647,7 338423,15 224777,3 80844,5 -1,-1 -1,-1 -1,-1
      //    60676,364 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1
      //    -1,-1 -1,-1 -1,-1 -1,-1 114421,78 -1,-1 -1,-1 5084,132 -1,-1 193950,60
      //    192544,108 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1 -1,-1 90581,21 -1,-1
      //    -1,-1 -1,-1 -1,-1 -1,-1 37105,11 -1,-1 -1,-1 101492,244 263961,112
      //    -1,-1 43169,1252
      // ";

      // had to run lynx through shell execture as Jagex Hiscores have curl issue due to TTL handshake when using Guzzle
      // therefore need to sanitise input to remove any illegal characters

      $sanitised_input = escapeshellarg($name);

      
      $data =  shell_exec ( "lynx -dump \"https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player={$sanitised_input}\" ");

      // dd("lynx -dump \"https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player={escapeshellcmd($name)}\" ");
     
      if(strpos($data, 'error404') !== false) {
        return false;
      } else {

      // use explode to split a string by a specified string in this case a space character
      $split_data = explode(" ", $data);

      // use array diff to remove certain unwanted values, here removing any value in array that is blank
      $split_data = array_diff($split_data, [""]);

      // because array_diff removes index 0 it turns array into key value pairs so need to re-write to new array with just values
      $temp = [];
      foreach ($split_data as $key => $value) {
        array_push($temp, $value);
      }

      $temp = array_slice($temp, 0, 24);
      $result = [];

      $skill_index = 0;
      foreach ($temp as $value) {
        $temp = explode(",", $value);

        // converting to int as this is the required data type, this also removes any unwanted \n characters from string
        $rank = (int)$temp[0];
        $level = (int)$temp[1];
        $exp = (int)$temp[2];
        $skill_name = $skills[$skill_index]['name'];

        $skill = ['rank'=> $rank, 'level'=>$level, 'exp'=>$exp];
        $result[$skill_name] = $skill;

        $skill_index++;
      }

      return $result;

      }
    }

    public function apiFetch($name) {

      $skills = $this->skillRepo->getAll();
      $formatted = [];

      $response = Http::get("https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player={$name}");
      $split = explode("\n", $response->body());

      // sample incase hiscores down
      // $split =["60645,2096,190897342","212122,96,10564680","243244,92,6662226","270910,99,13065228","174665,99,17599646","146860,99,16212427","99144,88,4500472","207155,97,10734464","58724,99,13427492","80293,95,9620970","139246,94,8703635","118341,91,5948703","301482,85,3267948","131336,85,3591852","71867,89,4858156","147159,82,2627707","108817,87,3984285","83609,85,3276155","91519,91,5907074","182280,91,6043762","4917,99,30436198","70014,86,3598467","188657,79,1875647","53753,88,4390148","-1,-1","-1,-1","-1,-1","418399,38","-1,-1","576511,3","449866,7","353539,16","206708,5","75212,7","-1,-1","-1,-1","-1,-1","68417,364","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","130148,78","-1,-1","-1,-1","3980,165","-1,-1","217841,60","198271,221","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","74584,27","-1,-1","-1,-1","-1,-1","-1,-1","-1,-1","29878,14","-1,-1","-1,-1","123244,244","239908,158","-1,-1","49113,1252",""];

      if ($response->successful()) {
        for($index =0; $index <= 23; $index++) {
          $info = explode(",", $split[$index]);
          $skill = $skills[$index]['name'];
          $formatted[$skill] = ['rank'=> (int)$info[0], 'level'=> (int)$info[1], 'exp'=> (int)$info[2]];
        }

        return $formatted;
      } else {
        return false;
      }



    }

    public function show($field, $criteria) {

    // acceptable fields and "username" and "id"
    if($field === 'username') {

      $stored = $this->personRepo->get($field, $criteria);      

      if($stored) {

        $time_diff = (strtotime(Carbon::now()) - strtotime($stored['updated_at']));
        

        if($time_diff < 10800) {

          return $this->personSkillRepo->getByUsername($criteria);

        // else below for if exists but out of date
        } else {

          $formatted_data = $this->apiFetch($criteria);

          $skills = $this->skillRepo->getAll();

          $updated_person = $this->personRepo->updateTimestamp($stored['id']);

          $updated = $this->personSkillRepo->updateAll($stored, $formatted_data, $skills);

          return $updated;  
        }

      // if does not exist, do API call and insert data
      } else {
        
        $formatted_data =  $this->apiFetch($criteria);

        // false is returned if API fetch fails
        if($formatted_data != false) {

          // as user does not exist, insert the new person record using the given username and return id
          $person_id = $this->personRepo->insert($criteria)['id'];

          // retrieve skills information from DB
          $skills = $this->skillRepo->getAll();

          // insert data
          $this->personSkillRepo->insertAll($person_id, $formatted_data, $skills);

          return $formatted_data;

        } else {
          return response(['error'=> 'the username was either invalid or the OSRS hiscores weren\'t reachable'], 422);
        }
      }


    } elseif($field === 'id')  {
      return $this->personRepo->get($field, $criteria);


    } else {
      return response(['error'=> 'please select provide a valid field'], 422);
    }

    }

  }
