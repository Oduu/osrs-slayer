<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LocationRepository;

class LocationController extends Controller
{

    public function __construct(LocationRepository $locationRepository) {
        
        $this->locationRepo = $locationRepository;
        
    }

    public function index() {
      return $this->locationRepo->getAll();
    }

    public function show($field, $criteria) {
    	return $this->locationRepo->get($field, $criteria);
    }
}
