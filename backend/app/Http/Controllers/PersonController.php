<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Repositories\PersonRepository;

class PersonController extends Controller
{

    public function __construct(PersonRepository $personRepository) {
        
        $this->personRepo = $personRepository;
        
    }

    public function index() {
      return $this->personRepo->getAll();
    }

    public function show($field, $criteria){

      $result = $this->personRepo->get($field, $criteria);

      if($result) {
        return response($result, 200);
      } else {
        return response(['error'=>'no data found'], 204);
      }
    }


}
