<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ItemRepository;
use App\Repositories\SkillRepository;
use App\Repositories\ItemRequirementRepository;
use GuzzleHttp\Client;

class ItemController extends Controller
{
    public function __construct(ItemRepository $itemRepository, SkillRepository $skillRepository, ItemRequirementRepository $itemRequirementRepository) {
    	$this->itemRepo = $itemRepository;
    	$this->skillRepo = $skillRepository;
    	$this->itemReqRepo = $itemRequirementRepository;
    }

    public function index() {
    	return $this->itemReqRepo->getAllWithReq();
    }

    public function show($field, $criteria) {
        
    	$item = $this->itemRepo->get($field, $criteria);

        return $this->itemReqRepo->getWithReq($item['id']);
    }



    // testing base64 image returns
    public function testimage() {

    	$str = 'iVBORw0KGgoAAAANSUhEUgAAACQAAAAgCAYAAAB6kdqOAAACPUlEQVR4Xs2Xz0sCQRTHe39Ch+4SnSIiunTp0KUOURBREUQQkQUlEhWViYiUlYgopSJKVPbLQxSKx+hfe/FmmmZnRitzdu0LAzs7677P+zGzz64uzwSo3+mACIKPVOr487pjAqzXK/j4WMS7ch4TiSgDGvL53ICSnusrUsBAKpUSlkoZPDuL4MbGyg+/aVkcolBIYTodZ0Yag/HoiDRdXV1gNLqPJ8dHNqMEmM8n2QtjsUMMhbZxbm6azTOZUw0K8P6+4DAOeHAQZJGyCpTLJnBhYcYRFT4EqA5FhqVxvq7ea0uAlxfn2kuBpU4Y6+npVqB0wxZhSDI1utcChGrLTJ+rUqNDxmu1JwckYLGYdpw5HoCpIefRoSKna0qpE+b5+cYbKClumHaPE0RESxS7xbr5TrxmZGpkOgUQnT1y3XWZEGYkAG9vcl/nj7pmTerOMiF0AV5fZ92C4h5Tsb691diO+p0RV6DkOZRMxtgQhfxzlEhqivXVFgXsqx0MruPAQP+nl3xMTU782mtLMCTASGQPx8fHNC8Bl5bmW4iSFfEULS7ONgg54ObmqvdAgcBaAxi+Rm2FR0DOA60RDL8vPhvmmlUBvr6UWRMmwJobbAbbluSuoUGN2O7OFmvGRkdHjD5Hf946DDXjdHBR70tzqhnaUX19vYpRsbPe3+vo9y+zZ0xYK9K9VedOmHg8zCI3PDRoPKO/tS2phs25MC7OIhPWU/G6Cod3Og0iBFitPtj+X9WuXKyXv+pfwXilD/dz/gBHQmUuAAAAAElFTkSuQmCC';

    	printf('<img src="data:image/png;base64,%s" />', $str);

    	return "sponge" ;
    }

}
