<?php

namespace App\Repositories;
use App\Models\Person;

class PersonRepository

{
	public function getAll() {
		return Person::select('id','username', 'created_at', 'updated_at')->get();
	}

	public function get($field, $criteria) {
		return Person::select('id','username', 'created_at', 'updated_at')->where($field, $criteria)->first();
	}

	public function insert($username) {
		$user = new Person;
		$user->username = $username;
		$user->save();

		return $this->get('username', $username);
	}

	public function updateTimestamp($person_id) {

		$person = $this->get('id', $person_id);

		$person->updated_at = date('Y-m-d H:i:s');

		// return $person;

		$person->save();

		return $this->get('id', $person_id);

	}

}