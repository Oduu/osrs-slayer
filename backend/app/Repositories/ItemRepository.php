<?php  

namespace App\Repositories;
use App\Models\Item;

class ItemRepository

{
	public function getAll() {
		return Item::select('id','name','equipable','wiki')->get();
	}

	public function get($field, $criteria) {
		return Item::select('id','name','equipable','wiki')->where($field, $criteria)->first();
	}

	public function massInsert($items) {
		Item::insert($items);

		return $this->getAll();
	}

	public function getIn($item_names) {
		// SELECT id, name, slot, equipable
		// FROM item
		// WHERE name IN ("Toolkit", "Prospector boots", "Dragonfire shield")
		return Item::whereIn('name', $item_names)->get();
	}

}

