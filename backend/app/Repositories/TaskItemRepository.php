<?php

namespace App\Repositories;
use App\Models\TaskItem;
use DB;

class TaskItemRepository 

{

	public function getAll() {
		return TaskItem::select('task_id', 'item_id','setup', 'equipped')->get();
	}

	public function get($field, $criteria) {
		return TaskItem::select('task_id','item_id','setup', 'equipped')->where($field, $criteria)->first();
	}

	public function insert_setup($items) {
		TaskItem::insert($items);
	}

	public function getWithNames($task_id) {


		$result = DB::table('task_item')
		->join('item', 'item.id', '=', 'task_item.item_id')
		->join('task', 'task.id', '=', 'task_item.task_id')
		->where('task_id', $task_id)
		->select('task_id', 'item_id', 'item.name AS item_name', 'task.name AS task_name', 'slot', 'setup', 'equipped')
		->orderBy('setup', 'desc')
		->get();

		if(!$result->isEmpty()) {
			$formatted = new Setup($result);
			return $formatted->formatSetup();
		} else {
			return null;
		}

	}

	public function numberOfSetups($task_id) {

		$result = DB::table('task_item')
		->join('item', 'item.id', '=', 'task_item.item_id')
		->join('task', 'task.id', '=', 'task_item.task_id')
		->where('task_id', $task_id)
		->select('setup')
		->orderBy('setup', 'desc')
		->first('setup');

		if($result) {
			return $result->setup;	
		} else {
			return 0;
		}

		

	}

}

class Setup {

	public $dbResult;
	public $setup;
	

	public function __construct($dbResult) {
		$this->dbResult = $dbResult;
	}

	public function formatSetup() {

		$formatted = [];
		$number_of_setups = ((array)$this->dbResult[0])['setup'];

		// return $this->dbResult;		

		for($x= 1; $x < $number_of_setups+1; $x++) {
			$formatted[$x] = [
				"equipped" => [
					"head" => null,
					"cape" => null,
					"neck" => null,
					"ammo" => null,
					"weapon" => null,
					"body" => null,
					"shield" => null,
					"legs" => null,
					"hands" => null,
					"feet" => null,
					"ring" => null
				],
				"inventory" => []
			];
		}

		foreach ($this->dbResult as $item) {
			// return $formatted[$item->setup];
			$setup_number = $item->setup;

			// if item is equipped
			if($item->equipped === 1) {
				$formatted[$item->setup]['equipped'][$item->slot] = ["name"=> $item->item_name, "id" => $item->item_id ];
			} else {
				array_push($formatted[$item->setup]['inventory'], ["name"=> $item->item_name, "id"=> $item->item_id ]);
			}
			
		}
		
		return $formatted;

	}


}