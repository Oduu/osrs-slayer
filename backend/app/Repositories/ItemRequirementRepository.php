<?php

namespace App\Repositories;
use App\Models\ItemRequirement;
use DB;

class ItemRequirementRepository

{
	public function getAll() {
		return ItemRequirement::select('item_id','skill_id','level')->get();
	}

	public function get($field, $criteria) {
		return ItemRequirement::select('item_id','skill_id','level')->where($field, $criteria)->get();
	}

	public function getWithReq($item_id) {
		$result = DB::table('item')
		->leftJoin('item_requirement', 'item_id', '=', 'item.id')
		->leftJoin('skill', 'skill.id', '=', 'item_requirement.skill_id')
		->where('item.id', $item_id)
		->select('item.id as item_id', 'item.name AS name', 'item.equipable', 'item.wiki', 'item_requirement.level', 'skill.name AS skill_name', 'skill.id AS skill_id', 'item.slot')
		->get();

		$formatted = [];

		foreach ($result as $item) {

			$formatted['item_id'] = $item->item_id;
			$formatted['name'] = $item->name;
			$formatted['equipable'] = $item->equipable;
			$formatted['wiki'] = $item->wiki;
			$formatted['slot'] = $item->slot;

			if($item->skill_name) {
				$formatted['requirements'][$item->skill_name] = $item->level;	
			} else {
				$formatted['requirements'] = null;
			}
			
			
		}

		return $formatted;
	}

	public function getAllWithReq() {
		$result = DB::table('item')
		->leftJoin('item_requirement', 'item_id', '=', 'item.id')
		->leftJoin('skill', 'skill.id', '=', 'item_requirement.skill_id')
		->select('item.id as item_id', 'item.name AS name', 'item.equipable', 'item.wiki', 'item_requirement.level', 'skill.name AS skill_name', 'skill.id AS skill_id', 'item.slot')
		->get();

		$formatted = [];

		foreach ($result as $item) {

			$exists = false;

			foreach ($formatted as $key => $stored_item) {
				if($stored_item['name'] == $item->name) {
					// $stored_item['requirements'] = "hello this is a test";
					$formatted[$key]['requirements'][$item->skill_name] = $item->level;
					$exists = true;
				} 
			}

			if(!$exists) {
				$test = [];

				$test['item_id'] = $item->item_id;
				$test['name'] = $item->name;
				$test['equipable'] = $item->equipable;
				$test['wiki'] = $item->wiki;
				$test['slot'] = $item->slot;

				if($item->skill_name) {
					$test['requirements'][$item->skill_name] = $item->level;	
				} else {
					$test['requirements'] = null;
				}

				array_push($formatted, $test);			
			}

			
			
		}

		return $formatted;
	}

	public function massInsert($requirements) {
		ItemRequirement::insert($requirements);
		
		return $this->getAll();
	}

}