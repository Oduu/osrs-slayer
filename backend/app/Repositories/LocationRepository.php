<?php  

namespace App\Repositories;
use App\Models\Location;

class LocationRepository

{
	public function getAll() {
		return Location::select('id','name')->get();
	}

	public function get($field, $criteria) {
		return Location::select('id','name')->where($field, $criteria)->first();
	}

}

