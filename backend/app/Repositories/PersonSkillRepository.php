<?php

namespace App\Repositories;
use App\Models\PersonSkill;
use DB;

class PersonSkillRepository

{
	public function getByUsername($username) {

		$formatted = [];

		$result = DB::table('person_skill')
		->join('person', 'person.id', '=', 'person_skill.person_id')
		->join('skill', 'skill.id', '=', 'person_skill.skill_id')
		->where('username', $username)
		->select('name', 'level', 'rank', 'experience')
		->get();

		

		foreach ($result as $skill) {
			$formatted[$skill->name] = ['rank'=> $skill->rank, 'level'=> $skill->level, 'exp'=>$skill->experience];
		}

		return $formatted;
	
	}

	public function insertAll($person_id, $data, $skills) {

		$rows = [];

		foreach ($data as $key => $value) {

			foreach ($skills as $skill) {
			  if($skill['name'] === $key) {
			    $skill_id = $skill['id'];
			  }
			}

			// to insert mass data needs to be in an array of associate arrays with each AA being a row in the table
			$row = [
			  'person_id'=> $person_id,
			  'skill_id'=>$skill_id,
			  'level'=>$value['level'],
			  'rank'=>$value['rank'],
			  'experience'=>$value['exp'],
			  'created_at'=>date('Y-m-d H:i:s'),
			  'updated_at'=>date('Y-m-d H:i:s')
			];

			array_push($rows, $row);

		}

		return PersonSkill::insert($rows);

	}

	// data required here is returned from API fetch method or getByUsername in this file
	public function updateAll($person, $data, $skills) {

		$rows = [];

		foreach ($data as $key => $value) {
			foreach ($skills as $skill) {
				if($skill['name'] === $key) {
					$skill_id = $skill['id'];
				}
			}

			$row = [

				"person_id"=> $person['id'],
				"skill_id"=>$skill_id,
				"level"=>$value['level'],
				"rank"=>$value['rank'],
				"experience"=>$value['exp'],
				"updated_at"=>date('Y-m-d H:i:s')

			];

			array_push($rows, $row);

			// would be better to update in one query instead of running this in a for each (or running a delete and re-insert but we're only ever going to be dealing with 20ish rows)
			$updated = PersonSkill::select('person_id','skill_id','level','rank','experience', 'updated_at')->where('person_id', $person['id'])->where('skill_id', $skill_id)->update($row);

		}

		$new_data = $this->getByUsername($person['username']);

		return $new_data;
	}

}