<?php

namespace App\Repositories;
use App\Models\Skill;

class SkillRepository

{
	public function getAll() {
		return Skill::select('id','name')->get();
	}

	public function get($field, $criteria) {
		return Skill::select('id','name')->where($field, $criteria)->first();
	}

}