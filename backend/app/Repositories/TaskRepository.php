<?php

namespace App\Repositories;
use App\Models\Task;

class TaskRepository

{
	public function getAll() {
		return Task::select('id','name')->get();
	}

	public function get($field, $criteria) {
		return Task::select('id', 'name')->where($field, $criteria)->first();
	}

}