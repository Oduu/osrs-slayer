<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskItem extends Model
{
    protected $table = 'task_item';

    protected $fillable = [

    	'task_id',
    	'item_id',
    	'setup'

    ];
}
