<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Prayer extends Model
{
    protected $table = "prayer";

    protected $fillable = [
    	"id",
    	"name",
    	"level"
    ];
}
