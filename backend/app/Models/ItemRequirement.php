<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemRequirement extends Model
{
	protected $table = 'item_requirement';

	protected $fillable = [
    	"item_id",
    	"skill_id",
    	"level",    	
    ];
	
}
