<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonSkill extends Model
{
	protected $table = "person_skill";

    protected $fillable = [
    	"person_id",
    	"skill_id",
    	"level",
    	"rank",
    	"experience",
    	"created_at",
    	"updated_at"
    ];
}
