<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['web']], function () {

	// Character
	Route::get('/character/', 'PersonController@index')->name("character.index");
	Route::get('/character/{field}/{criteria}', 'PersonController@show')->name("character.show");

	// Character Skill
	Route::get('/character/{field}/{criteria}/skill', 'PersonSkillController@show')->name("characterSkill.show");

	// Skill
	Route::get('/skill', 'SkillController@index')->name("skill.index");
	Route::get('/skill/{field}/{criteria}', 'SkillController@show')->name("skill.show");

	// Task
	Route::get('/task', 'TaskController@index')->name("task.index");
	Route::get('/task/{field}/{criteria}', 'TaskController@show')->name("task.show");

	// Item
	Route::get('/item', 'ItemController@index')->name("item.index");
	Route::get('/item/{field}/{criteria}', 'ItemController@show')->name("item.show");

	// Setups
	Route::get('/task/{field}/{criteria}/setup', 'TaskItemController@show')->name("taskItem.show");
	Route::post('/task/{field}/{criteria}/setup', 'TaskItemController@create')->name("taskItem.show");

	// Location
	Route::get('/location', 'LocationController@index')->name("location.index");
	Route::get('/location/{field}/{criteria}', 'LocationController@show')->name("location.show");


	// test routes
	Route::post('/test', 'TaskItemController@test')->name("taskItem.xxx");
	
	
});

