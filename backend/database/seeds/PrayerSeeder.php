<?php

use Illuminate\Database\Seeder;

class PrayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
		$prayers = [ 
			["name" => 'Thick Skin', "level" => 1 ],
			["name" => 'Burst of Strength', "level" => 4 ],
			["name" => 'Clarity of Thought', "level" => 7 ],
			["name" => 'Sharp Eye', "level" => 8 ],
			["name" => 'Mystic Will', "level" => 9 ],
			["name" => 'Rock Skin', "level" => 10 ],
			["name" => 'Superhuman Strength', "level" => 13 ],
			["name" => 'Improved Reflexes', "level" => 16 ],
			["name" => 'Rapid Heal', "level" => 22 ],
			["name" => 'Protect Item', "level" => 25 ],
			["name" => 'Hawk Eye', "level" => 26 ],
			["name" => 'Mystic Lore', "level" => 27 ],
			["name" => 'Steel Skin', "level" => 28 ],
			["name" => 'Ultimate Strength', "level" => 31 ],
			["name" => 'Incredible Reflexes', "level" => 34 ],
			["name" => 'Protect from Magic', "level" => 37 ],
			["name" => 'Protect from Missles', "level" => 40 ],
			["name" => 'Protect from Melee', "level" => 43 ],
			["name" => 'Eagle Eye', "level" => 44 ],
			["name" => 'Mystic Might', "level" => 45 ],
			["name" => 'Retribution', "level" => 46 ],
			["name" => 'Redemption', "level" => 49 ],
			["name" => 'Smite', "level" => 52 ],
			["name" => 'Preserve', "level" => 55 ],
			["name" => 'Chivalry', "level" => 60 ],
			["name" => 'Piety', "level" => 70 ],
			["name" => 'Rigour', "level" => 74 ],
			["name" => 'Augury', "level" => 77 ]];

		DB::table('prayer')->delete();
		$id = 1;
		foreach ($prayers as $prayer) {
			DB::table('prayer')->insert([
				'id' => $id,
				'name' => $prayer["name"],
				'level' => $prayer["level"]
			]);
			$id++;
		}

    }
}

