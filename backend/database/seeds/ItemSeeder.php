<?php

use Illuminate\Database\Seeder;
use App\Repositories\ItemRepository;
use App\Repositories\SkillRepository;
use App\Repositories\ItemRequirementRepository;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct(ItemRepository $itemRepository, SkillRepository $skillRepository, ItemRequirementRepository $itemRequirementRepository) {
    	$this->itemRepo = $itemRepository;
    	$this->skillRepo = $skillRepository;
    	$this->itemReqRepo = $itemRequirementRepository;
    }

    public function run()
    {
    	DB::table('item')->delete();
    	DB::table('item_requirement')->delete();

    	$path = storage_path('app/items-complete.json');
    	$file = file_get_contents($path);
    	$file_items = json_decode($file, true);

    	$items = [];
    	$requirements = [];
    	$skills = $this->skillRepo->getAll();
    	$unique_names = [];

    	foreach ($file_items as $obj) {

    		if(!in_array($obj['name'], $unique_names)) {

    			array_push($unique_names, $obj['name']);
	    		$details = [

	    			'id'=> $obj['id'],
	    			'name'=> $obj['name'],
	    			'equipable'=> $obj['equipable_by_player'],
	    			'wiki'=> $obj['wiki_url'],
	    			'slot'=> null

	    		];

	    		if($obj['equipable_by_player']) {
	    			$details['slot'] = $obj['equipment']['slot'];
	    		}

				if($obj['equipable_by_player'] && $obj['equipment']['requirements'] != null) {

	    			// if requirements exists get each requirement and add to details array
	    			foreach ($obj['equipment']['requirements'] as $skill => $level) {

	    				foreach ($skills as $db_skill) {
	    					if($db_skill['name'] == $skill) {
	    						$skill_id = $db_skill['id'];
	    					}
	    				}

	    				

	    				$requirement = [
		    				'item_id'=> $obj['id'],
		    				'skill_id'=> $skill_id,
		    				'level'=> $level
	    				];

	    				array_push($requirements, $requirement);
	    			}

	    		}

	    		array_push($items, $details);
	    	} else {
	    		continue;
	    	}


	    }

	    $chunked_items = array_chunk($items, 2000);
	    $chunked_reqs = array_chunk($requirements, 2000);

	    foreach ($chunked_items as $item_chunk) {
	    	$this->itemRepo->massInsert($item_chunk);
	    }

	   	foreach ($chunked_reqs as $req_chunk) {
	    	$this->itemReqRepo->massInsert($req_chunk);
	    }

    }

}
