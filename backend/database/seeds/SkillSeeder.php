<?php

use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $skills = ['overall', 'attack', 'defence', 'strength', 'hitpoints', 'ranged', 'prayer', 'magic', 'cooking', 'woodcutting', 'fletching',
                  'fishing', 'firemaking', 'crafting', 'smithing', 'mining', 'herblore', 'agility', 'thieving', 'slayer', 'farming', 'runecraft',
                  'hunter', 'construction'];

		DB::table('skill')->delete();

		$id = 1;
		foreach ($skills as $skill) {
			DB::table('skill')->insert([
				'id' => $id,
				'name' => $skill
			]);
			$id++;
		}

    }
}
