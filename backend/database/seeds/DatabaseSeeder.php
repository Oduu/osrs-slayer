<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('SkillSeeder');
        $this->command->info("Skill table seeded");
        $this->call('TaskSeeder');
        $this->command->info("Task table seedded");
        $this->call('ItemSeeder');
        $this->command->info("Item table seedded");
        $this->call('LocationSeeder');
        $this->command->info("Location table seedded");
        $this->call('PrayerSeeder');
        $this->command->info("Prayer table seedded");

    }
}
