<?php

use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		$file = file_get_contents('public/locations.txt');
		$locations = explode("\n", $file);

		DB::table('location')->delete();
		$id = 1;
		foreach ($locations as $location) {
			DB::table('location')->insert([
				'id' => $id,
				'name' => $location
			]);
			$id++;
		}


    }
}
