<?php

use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      // all tasks from https://oldschool.runescape.wiki/w/Slayer_task
      $tasks = ['Aberrant Spectres', 'Abyssal Demons', 'Adamant Dragons', 'Ankous', 'Aviansie', 'Bandits', 'Banshees', 'Basilisks', 'Bats',
      'Bears', 'Birds', 'Black Demons', 'Black Dragons', 'Black Knights', 'Bloodvelds', 'Blue Dragons', 'Brine Rats', 'Bronze Dragons',
      'Catablepon', 'Cave Bugs', 'Cave Crawlers', 'Cave Horrors', 'Cave Kraken', 'Cave Slimes', 'Chaos Druids', 'Cockatrices', 'Cows',
      'Crawling Hands', 'Crocodiles', 'Daggonoths', 'Dark Beasts', 'Dark Warriors', 'Dogs', 'Drakes', 'Dust Devils', 'Dwarfs', 'Earth Warriors', 
      'Elves', 'Ent', 'Fever Spiders', 'Fire Giants', 'Flesh Crawlers', 'Fossil Island Wyvern', 'Gargoyles', 'Ghosts', 'Ghouls', 'Goblins', 
      'Greater Demons', 'Green Dragons', 'Harpie Bug Swarms', 'Hellhounds', 'Hill Giant', 'Hobgoblins', 'Hydras', 'Ice Giants', 'Ice Warriors',
      'Icefiends', 'Infernal Mages', 'Iron Dragons', 'Jellies', 'Jungle Horros', 'Kalphites', 'Killerwatts', 'Kurasks', 'Lava Dragons', 'Lesser Demons',
      'Lizardmen', 'Lizards', 'Magic Axes', 'Mammoth', 'Minotaurs', 'Mithril Dragons', 'Mogres', 'Molanisks', 'Monkeys', 'Moss Giants',
      'Nechryael', 'Ogres', 'Otherwordly Beings', 'Pirate', 'Pyrefiends', 'Red Dragons', 'Revenants', 'Rockslugs', 'Rogues', 'Rune Dragons', 
      'Scabarites', 'Scorpions', 'Sea Snakes', 'Shades', 'Shadow Warriors', 'Skeletal Wyverns', 'Skeletons', 'Smoke Devils', 'Spiders', 
      'Sprititual Creatues', 'Steel Dragons', 'Suqahs', 'Terror Dogs', 'Trolls', 'Turoths', 'Vampyres', 'Wall Beasts', 'Waterfiends', 
      'Werewolves', 'Wolves', 'Wyrms', 'Zombies', 'Zygomites'];

    DB::table('task')->delete();

    $id = 1;
    foreach ($tasks as $task) {
      DB::table('task')->insert([
        'id' => $id,
        'name' => $task
      ]);
      $id++;
    }

    }
}
