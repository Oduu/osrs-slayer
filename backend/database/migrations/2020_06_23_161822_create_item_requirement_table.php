<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemRequirementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_requirement', function (Blueprint $table) {

            $table->integer('item_id')->unsigned();
            $table->unsignedBigInteger('skill_id');
            $table->foreign('item_id')->references('id')->on('item');
            $table->foreign('skill_id')->references('id')->on('skill');
            $table->integer('level');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_requirement');
    }
}
