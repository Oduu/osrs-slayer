<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {

            $table->integer('id')->unsigned(); //create field
            $table->primary('id'); //set field of id to primary key
            $table->string('name');
            $table->tinyInteger('equipable');
            $table->string('wiki',2083)->nullable();
            $table->string('slot')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
