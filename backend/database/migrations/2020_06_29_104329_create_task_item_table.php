<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_item', function (Blueprint $table) {

            $table->unsignedBigInteger('task_id');
            $table->integer('item_id')->unsigned();
            
            $table->foreign('item_id')->references('id')->on('item');
            $table->foreign('task_id')->references('id')->on('task');
            $table->integer('setup');
            $table->tinyInteger('equipped');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_item');
    }
}
